var callIPUrl = "http://192.168.0.6:3000/";
$(document).on('pageinit', function() {

    //
    // $.mobile.changePage("#manager_view_notices_page"); //To jump directly to that page
    //
    $("#loginBtn").on('click', function(event) {
        // $.mobile.changePage("#tenent_dashboard_page"); //To navigate to tenant dashboard page
        // $.mobile.changePage("#manager_dashboard_page"); //To navigate to manager dashboard page
    });

    //Tenant Side Basic Navigation Starts
    $("#tenent_card_report_new_issue").on('click', function(event) {
        $.mobile.changePage("#tenant_ticket_creation_page");
    });

    $("#tenent_card_list_all_issues").on('click', function(event) {
        $.mobile.changePage("#tenant_list_issue_page");
    });

    $("#tenant_card_view_news").on('click', function(event) {
        $.mobile.changePage("#tenant_view_notices_page");
    });

    $(".tenent_backToDashboardBtn").on('click', function(event) {
        $.mobile.changePage("#tenent_dashboard_page");
    });

    //Tenant Create Issue Form Buttons starts
    $("#clearTenentIssueFormBtn").on('click', function(event) {
        $(this).closest('form').find("input[type=text], input[type=date], textarea").val("");
    });

    $("#createIssueBtn").on('click', function(event) {
        // $.mobile.changePage("#tenant_ticket_creation_success_page");
    });

    //Tenant Create Issue Form Buttons ends
    //Tenant Side Basic Navigation Ends
    //
    //Building Manager Side Basic Navigation Starts
    $("#manager_view_pending_tickets").on('click', function(event) {
        $.mobile.changePage("#manager_pending_ticket_page");
    });

    $("#mark_as_solved").on('click', function(event) {
        $.mobile.changePage("#manager_solve_ticket_success_page");
    });

    $("#manager_create_news").on('click', function(event) {
        $.mobile.changePage("#manager_news_creation_page");
    });

    $("#manager_view_news").on('click', function(event) {
        $.mobile.changePage("#manager_view_notices_page");
    });

    $("#manager_view_solved_tickets").on('click', function(event) {
        $.mobile.changePage("#manager_list_solved_issue_page");
    });

    $(".manager_backToDashboardBtn").on('click', function(event) {
        $.mobile.changePage("#manager_dashboard_page");
    });
});