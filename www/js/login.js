$(document).on('pageinit', function() {

    //Form validation starts here
    $('#login_form').validate({
        onclick: false,
        onkeyup: false,
        rules: {
            username: {
                required: true
            },
            password: {
                required: true
            },
        },
        messages: {
            username: {
                required: "Username cannot be empty."
            },
            password: {
                required: "Password cannot be empty."
            },
        },
        errorElement: 'div',
        errorLabelContainer: '.errorTxt',
        errorPlacement: function(error, element) {
            console.log(error.text());
        },
        submitHandler: function(form) {
            performFormSubmitAction();
        }
    });
    //Form Validation Ends Here

    //Clicking on get login button 
    // $("#loginBtn").on('click', function(event) {
    function performFormSubmitAction() {
        var userName = $('#username').val();
        var passWord = $('#password').val();
        const url = callIPUrl + "login/";
        $.ajax({
            type: "POST",
            dataType: "json",
            url: url,
            data: {
                username: userName,
                password: passWord
            },
            success: function(data) {
                if (data.status === "false") {
                    alert("Invalid Username / Password.");
                } else {
                    if (data.status === "200" && data.userType === "Tenant") {
                        //Navigate to tenant dashboard
                        localStorage.setItem('fullName', data.fullName);
                        localStorage.setItem("loginUserType", data.userType);
                        $.mobile.changePage("#tenent_dashboard_page");
                    } else if (data.status === "200" && data.userType === "BuildingManager") {
                        //Navigate to Building Manager dashboard
                        localStorage.setItem('fullName', data.fullName);
                        localStorage.setItem("loginUserType", data.userType);
                        $.mobile.changePage("#manager_dashboard_page");
                    }
                }

            },
            complete: function(data) {

            },
            error: function(err) {
                alert("Error while inserting to DB" + err);
                console.log("Error while inserting to DB" + err);
            }
        });
    }
});