$(document).on('pageinit', function() {
    $("#tenant_logout, #manager_logout").on('click', function(event) {
        localStorage.removeItem("fullName");
        localStorage.removeItem("loginUserType");
        $.mobile.changePage("#login_page");
    });
});