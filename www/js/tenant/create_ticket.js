$(document).on('pageinit', function() {
    $("#tenentName").val(localStorage.getItem("fullName"));
    $('#tenentName').attr('readonly', true);
    //
    //Form validation starts here
    $('#tenant_create_ticket_form').validate({
        onclick: false,
        onkeyup: false,
        rules: {
            tenentName: {
                required: true
            },
            tenantIssueUrgencySelect: {
                required: true
            },
            problemDescription: {
                required: true
            },
            issueDate: {
                required: true
            },
        },
        messages: {
            tenentName: {
                required: "Tenant name cannot be empty."
            },
            tenantIssueUrgencySelect: {
                required: "Please select issue urgency."
            },
            problemDescription: {
                required: "Problem Description cannot be empty."
            },
            issueDate: {
                required: "Please select issue Date."
            },
        },
        errorElement: 'div',
        errorLabelContainer: '.errorTxt',
        errorPlacement: function(error, element) {
            console.log(error.text());
        },
        submitHandler: function(form) {
            performFormSubmitAction();
        }
    });
    //Form Validation Ends Here
    function performFormSubmitAction() {
        var tenentName = $('#tenentName').val();
        var tenantIssueUrgencySelect = $('#tenantIssueUrgencySelect').val();
        var problemDescription = $('#problemDescription').val();
        var issueDate = $('#issueDate').val();
        console.log(tenentName + tenantIssueUrgencySelect + problemDescription + issueDate);
        const url = callIPUrl + "tenantCreateTicket/";
        /*$.ajax({
            type: "POST",
            dataType: "json",
            url: url,
            data: {
                username: userName,
                password: passWord
            },
            success: function(data) {
                if (data.status === "false") {
                    alert("Invalid Username / Password.");
                } else {
                    if (data.status === "200" && data.userType === "Tenant") {
                        //Navigate to tenant dashboard
                        localStorage.setItem('fullName', data.fullName);
                        localStorage.setItem("loginUserType", data.userType);
                        $.mobile.changePage("#tenent_dashboard_page");
                    } else if (data.status === "200" && data.userType === "BuildingManager") {
                        //Navigate to Building Manager dashboard
                        localStorage.setItem('fullName', data.fullName);
                        localStorage.setItem("loginUserType", data.userType);
                        $.mobile.changePage("#manager_dashboard_page");
                    }
                }

            },
            complete: function(data) {

            },
            error: function(err) {
                alert("Error while inserting to DB" + err);
                console.log("Error while inserting to DB" + err);
            }
        });*/
    }
});